<?php
        require 'includes/header.php';

?>



<div class="container">
    <div style="margin-top: 5%">
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <h1 style="color: #6252f0; margin-right: 67%">Product Add</h1>
            <button type="submit" class="btn btn-primary" name="add-product">Save</button>
            <a class="btn btn-danger" style="max-height: 40px" role="button">Cancel</a>
        </div>
        <hr />
        <form method="post" action="/app/products/store.php">
            <div class="mb-3">
                <label for="formGroupExampleInput" class="form-label" class="sku">SKU</label>
                <input type="number" class="form-control" style="max-width: 30%" name="sku"
                    id="formGroupExampleInput" />
            </div>
            <div class="mb-3">
                <label for="formGroupExampleInput" class="form-label" name="name">Name</label>
                <input type="text" class="form-control" style="max-width: 30%" id="formGroupExampleInput" />
            </div>
            <div class="mb-3">
                <label for="formGroupExampleInput" class="form-label" name="price">Price</label>
                <input type="number" class="form-control" style="max-width: 30%" id="formGroupExampleInput" />
            </div>
            <label>Type Switcher
                <select class="type-switcher" name="type-switcher">
                    <option value="">Type Switcher...</option>
                    <option value="DVD">DVD</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                </select>
            </label>

            <div class="result"></div>
            <button type="submit" class="btn btn-primary" name="add-product">Save</button>
        </form>
    </div>



</div>

<?php  
echo '<script type="text/JavaScript">  
      const selectDropdown = document.querySelector(".type-switcher");

selectDropdown.addEventListener("change", (event) => {
  const result = document.querySelector(".result");
  result.textContent = `Make change here ${event.target.value}`;
});
     </script>'; 
?>